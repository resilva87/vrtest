package com.resilva87.vrtest.service.impl;

import com.resilva87.vrtest.exception.PropertyNotFoundException;
import com.resilva87.vrtest.math.Point2DImpl;
import com.resilva87.vrtest.math.Rectangle;
import com.resilva87.vrtest.model.Properties;
import com.resilva87.vrtest.model.Property;
import com.resilva87.vrtest.model.Province;
import com.resilva87.vrtest.repository.PropertyRepository;
import com.resilva87.vrtest.repository.ProvinceRepository;
import com.resilva87.vrtest.service.PropertyService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class PropertyServiceImplTests {

    private PropertyRepository propertyRepository;
    private ProvinceRepository provinceRepository;
    private PropertyService propertyService;
    private Property property;

    @Before
    public void setUp() {
        property = new Property("",1,"",1,1,1,1,20);
        propertyRepository = mock(PropertyRepository.class);
        provinceRepository = mock(ProvinceRepository.class);
        propertyService = new PropertyServiceImpl(propertyRepository, provinceRepository);
        when(provinceRepository.findContaining(any(Property.class))).thenReturn(
                Arrays.asList(new Province("p1", new Rectangle(new Point2DImpl(0, 1000), new Point2DImpl(600, 500)))));
    }

    @Test
    public void testFindByExistingId() {
        when(propertyRepository.findById(anyLong())).thenReturn(Optional.of(property));
        assertThat(propertyService.findById(1L)).isNotNull();
    }

    @Test(expected = PropertyNotFoundException.class)
    public void testFindByIdNotFound() {
        when(propertyRepository.findById(anyLong())).thenReturn(Optional.empty());
        propertyService.findById(2L);
    }

    @Test
    public void testFindByRangeSearch() {
        when(propertyRepository.rangeSearch(any(Rectangle.class))).thenReturn(Arrays.asList(property));
        Properties foundProperties = propertyService.findByRangeSearch(1, 1, 1, 1);
        assertThat(foundProperties.getTotalProperties()).isEqualTo(1);
        assertThat(foundProperties.getProperties()).hasSize(1).contains(property);
    }

    @Test
    public void testCreateProperty() {
        when(propertyRepository.create(any(Property.class))).thenReturn(property);
        assertThat(propertyService.create(property)).isNotNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreatePropertyOutsideAnyProvince() {
        when(provinceRepository.findContaining(any(Property.class))).thenReturn(Collections.emptyList());
        propertyService.create(property);
    }
}
