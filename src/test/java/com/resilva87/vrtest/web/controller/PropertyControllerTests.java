package com.resilva87.vrtest.web.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import com.resilva87.vrtest.configuration.WebConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PropertyControllerTests {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testReturn404WhenFindByIdNotFound() throws Exception {
        mockMvc.perform(get("/properties/0"))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("field", is("id")))
                .andExpect(jsonPath("code", is("message.error.id.notfound")));
    }

    @Test
    public void testReturn200WhenFindByIdReturns() throws Exception {
        mockMvc.perform(get("/properties/1"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("id", is(1)));
    }

    @Test
    public void testReturnDataFindByRangeSearch() throws Exception {
        mockMvc.perform(get("/properties?ax=0&ay=1000&bx=600&by=500")) // Gode province
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("totalProperties", is(1746)))
                .andExpect(jsonPath("properties", hasSize(1746)));
    }

    @Test
    public void testNoDataFromFindByRangeSearch() throws Exception {
        mockMvc.perform(get("/properties?ax=1401&ay=1000&bx=1500&by=0"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("totalProperties", is(0)))
                .andExpect(jsonPath("properties", hasSize(0)));
    }

    @Test
    public void testCreatingValidProperty() throws Exception {
        String jsonBody = "{\n" +
                "  \"x\": 499,\n" +
                "  \"y\": 700,\n" +
                "  \"title\": \"Imóvel código 1, com 5 quartos e 4 banheiros\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 4,\n" +
                "  \"baths\": 4,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("title", is("Imóvel código 1, com 5 quartos e 4 banheiros")))
                .andExpect(jsonPath("price", is(1250000)))
                .andExpect(jsonPath("description", is("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")))
                .andExpect(jsonPath("beds", is(4)))
                .andExpect(jsonPath("baths", is(4)))
                .andExpect(jsonPath("squareMeters", is(210)))
                .andExpect(jsonPath("id", notNullValue()))
                .andExpect(jsonPath("provinces", notNullValue()));
    }

    @Test
    public void testCreatingPropertyErrorTitleIsEmpty() throws Exception {
        String jsonBody = "{\n" +
                "  \"x\": 222,\n" +
                "  \"y\": 444,\n" +
                "  \"title\": \"\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 4,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("title")))
                .andExpect(jsonPath("$[0]code", is("message.validation.title.empty")));
    }

    @Test
    public void testCreatingPropertyErrorsPrice() throws Exception {
        // Price property is spelled wrong
        String jsonBody = "{\n" +
                "  \"x\": 499,\n" +
                "  \"y\": 700,\n" +
                "  \"title\": \"Imóvel código 1, com 5 quartos e 4 banheiros\",\n" +
                "  \"pricxe\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 4,\n" +
                "  \"baths\": 4,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("price")))
                .andExpect(jsonPath("$[0]code", is("message.validation.price.null")));

        // Price below minimum
        String jsonBody2 = "{\n" +
                "  \"x\": 499,\n" +
                "  \"y\": 700,\n" +
                "  \"title\": \"Imóvel código 1, com 5 quartos e 4 banheiros\",\n" +
                "  \"price\": 0,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 4,\n" +
                "  \"baths\": 4,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody2))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("price")))
                .andExpect(jsonPath("$[0]code", is("message.validation.price.minimum")));
    }

    @Test
    public void testCreatingPropertyErrorDescriptionIsEmpty() throws Exception {
        String jsonBody = "{\n" +
                "  \"x\": 222,\n" +
                "  \"y\": 444,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"\",\n" +
                "  \"beds\": 4,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("description")))
                .andExpect(jsonPath("$[0]code", is("message.validation.description.empty")));
    }

    @Test
    public void testCreatingPropertyErrorsLatitude() throws Exception {
        // Latitude (x) property is spelled wrong
        String jsonBody = "{\n" +
                "  \"xx\": 222,\n" +
                "  \"y\": 444,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 4,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("latitude")))
                .andExpect(jsonPath("$[0]code", is("message.validation.latitude.null")));

        // Latitude (x) below minimum
        String jsonBody2 = "{\n" +
                "  \"x\": -1,\n" +
                "  \"y\": 444,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 4,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody2))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("latitude")))
                .andExpect(jsonPath("$[0]code", is("message.validation.latitude.minimum")));

        // Latitude (x) above maximum
        String jsonBody3 = "{\n" +
                "  \"x\": 1401,\n" +
                "  \"y\": 444,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 4,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody3))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("latitude")))
                .andExpect(jsonPath("$[0]code", is("message.validation.latitude.maximum")));
    }

    @Test
    public void testCreatingPropertyErrorsLongitude() throws Exception {
        // Longitude (y) property is spelled wrong
        String jsonBody = "{\n" +
                "  \"x\": 222,\n" +
                "  \"yy\": 444,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 4,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("longitude")))
                .andExpect(jsonPath("$[0]code", is("message.validation.longitude.null")));

        // Longitude (y) below minimum
        String jsonBody2 = "{\n" +
                "  \"x\": 300,\n" +
                "  \"y\": -1,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 4,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody2))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("longitude")))
                .andExpect(jsonPath("$[0]code", is("message.validation.longitude.minimum")));

        // Longitude (y) above maximum
        String jsonBody3 = "{\n" +
                "  \"x\": 111,\n" +
                "  \"y\": 1001,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 4,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody3))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("longitude")))
                .andExpect(jsonPath("$[0]code", is("message.validation.longitude.maximum")));
    }

    @Test
    public void testCreatingPropertyErrorsBeds() throws Exception {
        // Beds property is spelled wrong
        String jsonBody = "{\n" +
                "  \"x\": 222,\n" +
                "  \"y\": 444,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"bedds\": 4,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("beds")))
                .andExpect(jsonPath("$[0]code", is("message.validation.beds.null")));

        // Number of beds below minimum
        String jsonBody2 = "{\n" +
                "  \"x\": 300,\n" +
                "  \"y\": 200,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 0,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody2))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("beds")))
                .andExpect(jsonPath("$[0]code", is("message.validation.beds.minimum")));

        // Number of beds above maximum
        String jsonBody3 = "{\n" +
                "  \"x\": 111,\n" +
                "  \"y\": 111,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 6,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody3))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("beds")))
                .andExpect(jsonPath("$[0]code", is("message.validation.beds.maximum")));
    }

    @Test
    public void testCreatingPropertyErrorsBaths() throws Exception {
        // Baths property is spelled wrong
        String jsonBody = "{\n" +
                "  \"x\": 222,\n" +
                "  \"y\": 444,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 4,\n" +
                "  \"batths\": 3,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("baths")))
                .andExpect(jsonPath("$[0]code", is("message.validation.baths.null")));

        // Number of baths below minimum
        String jsonBody2 = "{\n" +
                "  \"x\": 300,\n" +
                "  \"y\": 200,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 3,\n" +
                "  \"baths\": 0,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody2))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("baths")))
                .andExpect(jsonPath("$[0]code", is("message.validation.baths.minimum")));

        // Number of baths above maximum
        String jsonBody3 = "{\n" +
                "  \"x\": 111,\n" +
                "  \"y\": 111,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 3,\n" +
                "  \"baths\": 5,\n" +
                "  \"squareMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody3))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("baths")))
                .andExpect(jsonPath("$[0]code", is("message.validation.baths.maximum")));
    }

    @Test
    public void testCreatingPropertyErrorsSquareMeters() throws Exception {
        // Square meters property is spelled wrong
        String jsonBody = "{\n" +
                "  \"x\": 222,\n" +
                "  \"y\": 444,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 4,\n" +
                "  \"baths\": 3,\n" +
                "  \"squarexMeters\": 210\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("squareMeters")))
                .andExpect(jsonPath("$[0]code", is("message.validation.squareMeters.null")));

        // Property size below minimum
        String jsonBody2 = "{\n" +
                "  \"x\": 300,\n" +
                "  \"y\": 200,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 3,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 19\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody2))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("squareMeters")))
                .andExpect(jsonPath("$[0]code", is("message.validation.squareMeters.minimum")));

        // Property size above maximum
        String jsonBody3 = "{\n" +
                "  \"x\": 111,\n" +
                "  \"y\": 111,\n" +
                "  \"title\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"price\": 1250000,\n" +
                "  \"description\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\n" +
                "  \"beds\": 3,\n" +
                "  \"baths\": 3,\n" +
                "  \"squareMeters\": 241\n" +
                "}";
        mockMvc.perform(post("/properties")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody3))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]field", is("squareMeters")))
                .andExpect(jsonPath("$[0]code", is("message.validation.squareMeters.maximum")));
    }
}
