package com.resilva87.vrtest.math;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class RectangleTest{

    private Rectangle gode, ruja, jaby, scavy, groola, nova;

    @Before
    public void setUp() {
        gode = new Rectangle(new Point2DImpl(0, 1000), new Point2DImpl(600, 500));
        ruja = new Rectangle(new Point2DImpl(400, 1000), new Point2DImpl(1100, 500));
        jaby = new Rectangle(new Point2DImpl(1100, 1000), new Point2DImpl(1400, 500));
        scavy = new Rectangle(new Point2DImpl(0, 500), new Point2DImpl(600, 0));
        groola = new Rectangle(new Point2DImpl(600, 500), new Point2DImpl(800, 0));
        nova = new Rectangle(new Point2DImpl(800, 500), new Point2DImpl(1400, 0));

    }

    @Test
    public void testComparisonDifferentXSameY() {
        assertThat(Rectangle.defaultComparator.compare(gode, ruja)).isEqualTo(-1);
        assertThat(Rectangle.defaultComparator.compare(jaby, ruja)).isEqualTo(1);
        assertThat(Rectangle.defaultComparator.compare(ruja, jaby)).isEqualTo(-1);
        assertThat(Rectangle.defaultComparator.compare(groola, scavy)).isEqualTo(1);
        assertThat(Rectangle.defaultComparator.compare(nova, groola)).isEqualTo(1);
    }

    @Test
    public void testComparisonChangeInY() {
        assertThat(Rectangle.defaultComparator.compare(gode, nova)).isEqualTo(1);
        assertThat(Rectangle.defaultComparator.compare(nova, gode)).isEqualTo(-1);
    }

    @Test
    public void testComparisonSameXDifferentY() {
        assertThat(Rectangle.defaultComparator.compare(gode, scavy)).isEqualTo(1);
        assertThat(Rectangle.defaultComparator.compare(scavy, gode)).isEqualTo(-1);
    }

    @Test
    public void testSameXAndY() {
        assertThat(Rectangle.defaultComparator.compare(jaby, jaby)).isEqualTo(0);
    }

    @Test
    public void testPointInsideRectangle() {
        assertThat(gode.contains(new Point2DImpl(300, 700))).isTrue();
        assertThat(scavy.contains(new Point2DImpl(600, 500))).isTrue();
    }

    @Test
    public void testPointOutsideRectangle() {
        assertThat(ruja.contains(new Point2DImpl(1101, 700))).isFalse();
        assertThat(nova.contains(new Point2DImpl(799, 200))).isFalse();
    }

    @Test
    public void testEquality() {
        assertThat(gode).isNotEqualTo(ruja);
        assertThat(ruja).isEqualTo(new Rectangle(new Point2DImpl(400, 1000), new Point2DImpl(1100, 500)));
    }
}
