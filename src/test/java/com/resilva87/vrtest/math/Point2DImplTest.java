package com.resilva87.vrtest.math;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Comparator;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class Point2DImplTest {

    private Comparator<Point2D> byX, byY;

    @Before
    public void setUp() {
        byX = Point2D.getComparator(Axis.X);
        byY = Point2D.getComparator(Axis.Y);
    }

    @Test
    public void testComparisonByX() {
        Point2DImpl aPoint = new Point2DImpl(1, 1);
        Point2DImpl otherPoint = new Point2DImpl(2, 1);
        assertThat(byX.compare(otherPoint, aPoint)).isEqualTo(1);
        assertThat(byX.compare(aPoint, otherPoint)).isEqualTo(-1);
        assertThat(byX.compare(aPoint, aPoint)).isEqualTo(0);
    }

    @Test
    public void testComparisonByY() {
        Point2DImpl aPoint = new Point2DImpl(1, 1);
        Point2DImpl otherPoint = new Point2DImpl(1, 2);
        assertThat(byY.compare(otherPoint, aPoint)).isEqualTo(1);
        assertThat(byY.compare(aPoint, otherPoint)).isEqualTo(-1);
        assertThat(byY.compare(aPoint, aPoint)).isEqualTo(0);
    }

    @Test
    public void testEquality() {
        Point2DImpl aPoint = new Point2DImpl(1, 1);
        Point2DImpl bPoint = new Point2DImpl(1, 2);
        Point2DImpl cPoint = new Point2DImpl(1, 1);
        assertThat(aPoint).isNotEqualTo(bPoint);
        assertThat(aPoint).isEqualTo(cPoint);
    }
}
