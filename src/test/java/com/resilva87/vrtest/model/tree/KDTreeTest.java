package com.resilva87.vrtest.model.tree;

import com.resilva87.vrtest.math.Point2D;
import com.resilva87.vrtest.math.Point2DImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class KDTreeTest {

    @Test
    public void testCreateKDTreeFromEmptyList() {
        KDTree kdTree = KDTree.empty();
        assertEquals(0, kdTree.size());
        assertThat(kdTree.isEmpty()).isTrue();
    }

    @Test
    public void testCreateEmptyKDTree() {
        KDTree kdTree = KDTree.empty();
        assertEquals(0, kdTree.size());
    }

    @Test
    public void testCreateKDTreeWithOneDimension() {
        Point2D point2D = new Point2DImpl(1, 1);
        Node node = new Node(point2D, null, null);
        KDTree kdTree = KDTree.fromPoints(Arrays.asList(point2D));
        assertEquals(1, kdTree.size());
        assertThat(kdTree.root()).isEqualTo(node);
    }

    @Test
    public void testCreateKDTreeWithTwoDimensions() {
        Point2D aPoint = new Point2DImpl(5, 25);
        Point2D bPoint = new Point2DImpl(10, 12);
        Point2D cPoint = new Point2DImpl(35, 45);
        Node rightNode = new Node(cPoint, null, null);
        Node leftNode = new Node(aPoint, null, null);
        Node rootNode = new Node(bPoint, leftNode, rightNode);
        KDTree kdTree = KDTree.fromPoints(Arrays.asList(aPoint, bPoint, cPoint));
        assertEquals(3, kdTree.size());
        assertThat(kdTree.root()).isEqualTo(rootNode);
    }

    @Test
    public void testAddPointToEmptyTree() {
        KDTree kdTree = KDTree.empty();
        Point2D aPoint = new Point2DImpl(0, 0);
        Node aNode = new Node(aPoint, null, null);
        kdTree.insert(aPoint);
        assertEquals(1, kdTree.size());
        assertThat(kdTree.root()).isEqualTo(aNode);
    }

    @Test
    public void testInsertInNonEmptyTree() {
        Point2D aPoint = new Point2DImpl(5, 25);
        Point2D bPoint = new Point2DImpl(10, 12);
        Point2D cPoint = new Point2DImpl(35, 45);
        Point2D dPoint = new Point2DImpl(22, 37);
        Point2D ePoint = new Point2DImpl(-1, 8);
        Point2D fPoint = new Point2DImpl(-7, 4);
        Point2D gPoint = new Point2DImpl(17, 21);
        Point2D hPoint = new Point2DImpl(32, 44);
        Point2D iPoint = new Point2DImpl(20, 0);
        KDTree kdTree = KDTree.fromPoints(Arrays.asList(aPoint, bPoint, cPoint, dPoint, ePoint, fPoint, gPoint,
                hPoint, iPoint));
        Point2D jPoint = new Point2DImpl(23, 43);
        Node inserted = kdTree.insert(jPoint);
        Node eNode = new Node(ePoint, new Node(fPoint), null);
        Node bNode = new Node(bPoint, eNode, new Node(aPoint));
        Node dNode = new Node(dPoint, new Node(iPoint), new Node(jPoint));
        Node hNode = new Node(hPoint, dNode, new Node(cPoint));
        Node root = new Node(gPoint, bNode, hNode);
        assertNotNull(inserted);
        assertEquals(10, kdTree.size());
        assertThat(kdTree.root()).isEqualTo(root);
    }

    @Test
    public void testInsertSamePointMultipleTimes() {
        KDTree kdTree = KDTree.empty();
        Point2D aPoint = new Point2DImpl(5, 25);
        for(int i = 0; i < 100; i++){
            kdTree.insert(aPoint);
        }
        assertThat(kdTree.size()).isEqualTo(100);
    }



}
