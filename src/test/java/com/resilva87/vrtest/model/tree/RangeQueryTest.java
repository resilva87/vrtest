package com.resilva87.vrtest.model.tree;

import com.resilva87.vrtest.math.Point2D;
import com.resilva87.vrtest.math.Point2DImpl;
import com.resilva87.vrtest.math.Rectangle;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class RangeQueryTest {

    private Point2D point22;
    private Point2D point55;
    private Point2D point77;
    private Point2D point34;
    private Point2D point35;
    private Point2D point53;
    private Point2D point84;
    private Point2D point88;
    private Point2D point91;
    private Point2D point117;
    private Point2D point51;
    private Point2D point95;
    private Point2D point24;
    private Point2D point47;
    private Point2D point63;
    private Point2D point128;
    private Point2D point12;
    private Point2D point74;
    private Point2D point106;

    private KDTree kdTree, fullKdTree, fullKdTree2, kdTree10Points;

    @Before
    public void setUp() {
        point22 = new Point2DImpl(2, 2);
        point55 = new Point2DImpl(5, 5);
        point77 = new Point2DImpl(7, 7);
        point88 = new Point2DImpl(8, 8);
        point34 = new Point2DImpl(3, 4);
        point35 = new Point2DImpl(3, 5);
        point53 = new Point2DImpl(5, 3);
        point84 = new Point2DImpl(8, 4);
        point91 = new Point2DImpl(9, 1);
        point117 = new Point2DImpl(11, 7);
        point51 = new Point2DImpl(5, 1);
        point95 = new Point2DImpl(9, 5);
        point24 = new Point2DImpl(2, 4);
        point47 = new Point2DImpl(4, 7);
        point63 = new Point2DImpl(6, 3);
        point128 = new Point2DImpl(12, 8);
        point12 = new Point2DImpl(1, 2);
        point74 = new Point2DImpl(7, 4);
        point106 = new Point2DImpl(10, 6);
        kdTree = KDTree.fromPoints(Arrays.asList(point22, point55, point88));
        fullKdTree = KDTree.fromPoints(
            Arrays.asList(point35, point53, point77, point84, point91, point117));
        fullKdTree2 = KDTree.fromPoints(
            Arrays.asList(point34, point53, point77, point84, point91, point117));
        kdTree10Points = KDTree.empty();
        kdTree10Points.insert(point51);
        kdTree10Points.insert(point35);
        kdTree10Points.insert(point95);
        kdTree10Points.insert(point24);
        kdTree10Points.insert(point47);
        kdTree10Points.insert(point63);
        kdTree10Points.insert(point128);
        kdTree10Points.insert(point12);
        kdTree10Points.insert(point74);
        kdTree10Points.insert(point106);
    }

    @Test
    public void testOnlyRootContained() {
        RangeQuery rangeQuery = new RangeQuery(new Rectangle(new Point2DImpl(4, 7),
                                                                new Point2DImpl(7, 4)));
        List<Point2D> pointsInRange = rangeQuery.search(kdTree);
        assertThat(pointsInRange).hasSize(1).contains(point55);
    }

    @Test
    public void testOnlyLeftContained() {
        RangeQuery rangeQuery = new RangeQuery(new Point2DImpl(1, 3), new Point2DImpl(1, 1),
                                                  new Point2DImpl(3, 1),
                                                  new Point2DImpl(3, 3));
        List<Point2D> pointsInRange = rangeQuery.search(kdTree);
        assertThat(pointsInRange).hasSize(1).contains(point22);
    }

    @Test
    public void testOnlyRightContained() {
        RangeQuery rangeQuery = new RangeQuery(new Point2DImpl(6, 9), new Point2DImpl(6, 6),
                                                  new Point2DImpl(9, 6),
                                                  new Point2DImpl(9, 9));
        List<Point2D> pointsInRange = rangeQuery.search(kdTree);
        assertThat(pointsInRange).hasSize(1).contains(point88);
    }

    @Test
    public void testRootAndLeftSubTreeContained() {
        RangeQuery rangeQuery = new RangeQuery(new Point2DImpl(1, 6), new Point2DImpl(1, 1),
                                                  new Point2DImpl(6, 1),
                                                  new Point2DImpl(6, 6));
        List<Point2D> pointsInRange = rangeQuery.search(kdTree);
        assertThat(pointsInRange).hasSize(2).contains(point22, point55);
    }

    @Test
    public void testRootAndRightSubTreeContained() {
        RangeQuery rangeQuery = new RangeQuery(new Point2DImpl(4, 9), new Point2DImpl(4, 4),
                                                  new Point2DImpl(9, 4),
                                                  new Point2DImpl(9, 9));
        List<Point2D> pointsInRange = rangeQuery.search(kdTree);
        assertThat(pointsInRange).hasSize(2).contains(point88, point55);
    }

    @Test
    public void testRangeBetweenSubtrees() {
        RangeQuery rangeQuery = new RangeQuery(new Point2DImpl(7, 8), new Point2DImpl(7, 0),
                                                  new Point2DImpl(10, 0),
                                                  new Point2DImpl(10, 8));
        List<Point2D> pointsInRange = rangeQuery.search(fullKdTree);
        assertThat(pointsInRange).hasSize(3).contains(point77, point84, point91);
    }

    @Test
    public void testRangeInLeftSubtree() {
        RangeQuery rangeQuery = new RangeQuery(new Point2DImpl(3, 6), new Point2DImpl(3, 3),
                                                  new Point2DImpl(6, 3),
                                                  new Point2DImpl(6, 6));
        List<Point2D> pointsInRange = rangeQuery.search(fullKdTree);
        assertThat(pointsInRange).hasSize(2).contains(point53, point35);
    }

    @Test
    public void testRangeBetweenSubtreeWithNodesAndLeaves() {
        RangeQuery rangeQuery = new RangeQuery(new Point2DImpl(3, 7), new Point2DImpl(3, 5),
                                                  new Point2DImpl(11, 5),
                                                  new Point2DImpl(11, 7));
        List<Point2D> pointsInRange = rangeQuery.search(fullKdTree);
        assertThat(pointsInRange).hasSize(3).contains(point117, point35, point77);
    }

    @Test
    public void testRangeBetweenSubtreeWithNodesAndLeavesExcludingLeaf() {
        RangeQuery rangeQuery = new RangeQuery(new Point2DImpl(6, 7), new Point2DImpl(6, 5),
                                                  new Point2DImpl(11, 5),
                                                  new Point2DImpl(11, 7));
        List<Point2D> pointsInRange = rangeQuery.search(fullKdTree2);
        assertThat(pointsInRange).hasSize(2).contains(point117, point77);
    }

    @Test
    public void testAllPointsInRange() {
        List<Point2D> pointsInRange = new RangeQuery(new Point2DImpl(0, 8), new Point2DImpl(0, 0),
                                                        new Point2DImpl(12, 0),
                                                        new Point2DImpl(12, 8))
                                          .search(kdTree10Points);
        assertThat(pointsInRange).hasSize(10).contains(point51, point35, point95, point24, point47,
            point63, point128, point12, point74, point106);
    }

    @Test
    public void testNoPointsInRange() {
        List<Point2D> pointsInRange = new RangeQuery(new Point2DImpl(6, 10), new Point2DImpl(6, 7),
                                                        new Point2DImpl(11, 7), new Point2DImpl(11,
                                                                                                   10))
                                          .search(kdTree10Points);
        assertThat(pointsInRange).isEmpty();
    }

    @Test
    public void testWholeRightSubtreeInRange() {
        List<Point2D> pointsInRange = new RangeQuery(new Point2DImpl(5, 8), new Point2DImpl(5, 1),
                                                        new Point2DImpl(12, 1), new Point2DImpl(12,
                                                                                                   8))
                                          .search(kdTree10Points);
        assertThat(pointsInRange).hasSize(6).contains(point51, point95, point63, point128,
            point74, point106);
    }

    @Test
    public void testWholeLeftSubtreeInRange() {
        List<Point2D> pointsInRange = new RangeQuery(new Point2DImpl(1, 7), new Point2DImpl(1, 1),
                                                        new Point2DImpl(5, 1), new Point2DImpl(5,
                                                                                                  7))
                                          .search(kdTree10Points);
        assertThat(pointsInRange).hasSize(5).contains(point51, point35, point24, point47,
            point12);
    }

    @Test
    public void testRangeQueryEmptyTree() {
        List<Point2D> pointsInRange = new RangeQuery(new Rectangle(new Point2DImpl(1, 1),
                                                               new Point2DImpl(2, 2)))
                                   .search(KDTree.empty());
        assertThat(pointsInRange).isEmpty();
    }

    @Test
    public void testRangeQueryMultiplePointsInTheSamePosition() {
        for(int i = 0; i < 100; i++) {
            kdTree10Points.insert(point63);
        }
        List<Point2D> pointsInRange = new RangeQuery(new Rectangle(point47,
                                                                      point63))
                                          .search(kdTree10Points);
        assertThat(pointsInRange).hasSize(102);
    }

}
