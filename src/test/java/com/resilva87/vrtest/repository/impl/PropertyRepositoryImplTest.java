package com.resilva87.vrtest.repository.impl;

import com.resilva87.vrtest.math.Point2DImpl;
import com.resilva87.vrtest.math.Rectangle;
import com.resilva87.vrtest.model.Property;
import com.resilva87.vrtest.repository.PropertyRepository;
import com.resilva87.vrtest.repository.ProvinceRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class PropertyRepositoryImplTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private PropertyRepository propertyRepository;
    private ProvinceRepository provinceRepository;

    private Property p1, p2, p3;

    @Before
    public void setUp() {
        propertyRepository = new PropertyRepositoryImpl(provinceRepository);

        p1 = new Property("", 1, ",", 1, 1, 1, 1, 20);
        p2 = new Property("", 1, ",", 2, 2, 1, 1, 20);
        p3 = new Property("", 1, ",", 3, 3, 1, 1, 20);
    }

    @Test
    public void testInsertNewNode() {
        assertThat(propertyRepository.findById(p1.getId())).isEmpty();
        assertThat(propertyRepository.findById(p2.getId())).isEmpty();
        assertThat(propertyRepository.findById(p3.getId())).isEmpty();
        propertyRepository.create(p1);
        propertyRepository.create(p2);
        propertyRepository.create(p3);
        assertThat(propertyRepository.findById(p1.getId())).isNotEmpty();
        assertThat(propertyRepository.findById(p2.getId())).isNotEmpty();
        assertThat(propertyRepository.findById(p3.getId())).isNotEmpty();
    }

    @Test
    public void testLoadProperties() {
        propertyRepository.load(Arrays.asList(p1, p2, p3));
        assertThat(propertyRepository.findById(p1.getId())).isNotEmpty();
        assertThat(propertyRepository.findById(p2.getId())).isNotEmpty();
        assertThat(propertyRepository.findById(p3.getId())).isNotEmpty();
    }

    @Test
    public void testRangeSearch() {
        propertyRepository.load(Arrays.asList(p1, p2, p3));
        List<Property> properties = propertyRepository.rangeSearch(
            new Rectangle(new Point2DImpl(0, 3), new Point2DImpl(3,
                                                                    0)));
        assertThat(properties).hasSize(3).contains(p1, p2, p3);
    }
}
