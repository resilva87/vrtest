package com.resilva87.vrtest.repository.impl;

import com.resilva87.vrtest.math.Point2DImpl;
import com.resilva87.vrtest.math.Rectangle;
import com.resilva87.vrtest.model.Property;
import com.resilva87.vrtest.model.Province;
import com.resilva87.vrtest.repository.ProvinceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class ProvinceRepositoryImplTest {

    private ProvinceRepository provinceRepository;

    private Province p1, p2, p3, p4, p5, p6;

    @Before
    public void setUp() {
        provinceRepository = new ProvinceRepositoryImpl();
        p1 = new Province("p1", new Rectangle(new Point2DImpl(0, 1000), new Point2DImpl(600, 500)));
        p2 = new Province("p2", new Rectangle(new Point2DImpl(400, 1000), new Point2DImpl(1100, 500)));
        p3 = new Province("p3", new Rectangle(new Point2DImpl(1100, 1000), new Point2DImpl(1400, 500)));
        p4 = new Province("p4", new Rectangle(new Point2DImpl(0, 500), new Point2DImpl(600, 0)));
        p5 = new Province("p5", new Rectangle(new Point2DImpl(600, 500), new Point2DImpl(800, 0)));
        p6 = new Province("p6", new Rectangle(new Point2DImpl(800, 500), new Point2DImpl(1400, 0)));
        provinceRepository.insert(p1);
        provinceRepository.insert(p2);
        provinceRepository.insert(p3);
        provinceRepository.insert(p4);
        provinceRepository.insert(p5);
        provinceRepository.insert(p6);
    }

    @Test
    public void testPropertyInSingleProvince() {
        Property aProperty = new Property("a", 4, "", 399, 640, 1, 1, 20);
        assertThat(provinceRepository.findContaining(aProperty)).hasSize(1).contains(p1);
    }

    @Test
    public void testPropertyInMoreThanOneProvince() {
        Property aProperty = new Property("a", 4, "", 450, 640, 1, 1, 20);
        assertThat(provinceRepository.findContaining(aProperty)).hasSize(2).contains(p1, p2);
    }

    @Test
    public void testPropertyInNoProvince() {
        Property aProperty = new Property("a", 4, "", 1401, 1001, 1, 1, 20);
        assertThat(provinceRepository.findContaining(aProperty)).isEmpty();
    }

}
