package com.resilva87.vrtest.repository;

import com.resilva87.vrtest.math.Rectangle;
import com.resilva87.vrtest.model.Property;

import java.util.List;
import java.util.Optional;

public interface PropertyRepository {
    Property create(Property newProperty);
    void load(List<Property> properties);
    Optional<Property> findById(Long id);
    List<Property> rangeSearch(Rectangle boundary);
}
