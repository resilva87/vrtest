package com.resilva87.vrtest.repository.impl;

import com.resilva87.vrtest.math.Rectangle;
import com.resilva87.vrtest.model.Property;
import com.resilva87.vrtest.model.tree.KDTree;
import com.resilva87.vrtest.model.tree.Node;
import com.resilva87.vrtest.model.tree.RangeQuery;
import com.resilva87.vrtest.repository.PropertyRepository;
import com.resilva87.vrtest.repository.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
class PropertyRepositoryImpl implements PropertyRepository {

    private final AtomicLong idSequence;

    private final Map<Long, Node> existingIds;
    private KDTree twoDTree;

    @Autowired
    PropertyRepositoryImpl(ProvinceRepository provinceRepository) {
        this.idSequence = new AtomicLong();
        this.existingIds = new HashMap<>();
        twoDTree = KDTree.empty();
    }

    @Override
    public Property create(Property newProperty) {
        assert newProperty != null;
        setId(newProperty);
        Node newNode = twoDTree.insert(newProperty);
        existingIds.put(newProperty.getId(), newNode);
        return newProperty;
    }

    @Override
    public void load(List<Property> properties) {
        assert properties != null;
        properties.forEach(this::setId);
        twoDTree = KDTree.fromPoints(properties);
        twoDTree.traverse(node -> {
            Property property = (Property) node.getValue();
            existingIds.put(property.getId(), node);
        });
    }

    private void setId(Property property) {
        property.setId(idSequence.incrementAndGet());
    }

    @Override
    public Optional<Property> findById(Long id) {
        return Optional.ofNullable(existingIds.get(id)).map(n -> (Property) n.getValue());
    }

    @Override
    public List<Property> rangeSearch(Rectangle boundary) {
        if (twoDTree.isEmpty()) {
            return Collections.emptyList();
        }
        RangeQuery rangeQuery = new RangeQuery(boundary);
        return rangeQuery.search(twoDTree).stream().map(p -> (Property) p).collect(Collectors.toList());
    }
}
