package com.resilva87.vrtest.repository.impl;

import com.resilva87.vrtest.math.Point2D;
import com.resilva87.vrtest.math.Rectangle;
import com.resilva87.vrtest.model.Property;
import com.resilva87.vrtest.model.Province;
import com.resilva87.vrtest.repository.ProvinceRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
class ProvinceRepositoryImpl implements ProvinceRepository {

    private List<Province> provinces;

    ProvinceRepositoryImpl() {
        this.provinces = new ArrayList<>();
    }

    @Override
    public void insert(Province province) {
        provinces.add(province);
        provinces.sort(Province.defaultComparator);
    }

    @Override
    public List<Province> findContaining(Property property) {
        int startingIndexOfTargetPoint = getStartingIndexOfTargetPoint(property);
        int endingIndexOfTargetPoint = getEndingIndexOfTargetPoint(property);
        if(startingIndexOfTargetPoint < 0 || endingIndexOfTargetPoint < 0){
            return Collections.emptyList();
        }
        if (startingIndexOfTargetPoint == endingIndexOfTargetPoint) {
            return Collections.singletonList(provinces.get(startingIndexOfTargetPoint));
        }
        return
            provinces.subList(startingIndexOfTargetPoint,
                endingIndexOfTargetPoint + 1);
    }

    private int getStartingIndexOfTargetPoint(Point2D point2D) {
        int low = 0, high = provinces.size() - 1;
        // get the start index of target number
        int startIndex = -1;
        while (low <= high) {
            int mid = (high - low) / 2 + low;
            Rectangle midRectangle = provinces.get(mid).getBoundary();
            if (rectangleIsGreater(midRectangle, point2D)) {
                high = mid - 1;
            } else if (point2DInBounds(midRectangle, point2D)) {
                startIndex = mid;
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return startIndex;
    }

    private int getEndingIndexOfTargetPoint(Point2D point2D) {
        // get the end index of target number
        int low = 0, high = provinces.size() - 1;
        int endIndex = -1;
        while (low <= high) {
            int mid = (high - low) / 2 + low;
            Rectangle midRectangle = provinces.get(mid).getBoundary();
            if (rectangleIsGreater(midRectangle, point2D)) {
                high = mid - 1;
            } else if (point2DInBounds(midRectangle, point2D)) {
                endIndex = mid;
                low = mid + 1;
            } else {
                low = mid + 1;
            }
        }
        return endIndex;
    }

    private boolean point2DInBounds(Rectangle midRectangle, Point2D point2D) {
        return midRectangle.contains(point2D);
    }

    private boolean rectangleIsGreater(Rectangle midRectangle, Point2D point2D) {
        return point2D.y() <= midRectangle.getUpperLeft().y()
                   && midRectangle.getUpperLeft().x() > point2D.x();
    }
}
