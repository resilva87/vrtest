package com.resilva87.vrtest.repository;

import com.resilva87.vrtest.model.Property;
import com.resilva87.vrtest.model.Province;
import java.util.List;

public interface ProvinceRepository {
    void insert(Province province);
    List<Province> findContaining(Property property);
}
