package com.resilva87.vrtest.loaders;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.resilva87.vrtest.math.Point2DImpl;
import com.resilva87.vrtest.math.Rectangle;
import com.resilva87.vrtest.model.Province;
import com.resilva87.vrtest.repository.ProvinceRepository;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class ProvincesLoader implements CommandLineRunner {

    private static final String UPPER_LEFT_FIELD = "upperLeft";
    private static final String BOTTOM_RIGHT_FIELD = "bottomRight";
    private static final String X_VALUE = "x";
    private static final String Y_VALUE = "y";
    public static final String PROVINCES_JSON = "provinces.json";

    private final Logger logger;
    private final ProvinceRepository provinceRepository;

    @Autowired
    public ProvincesLoader(ProvinceRepository provinceRepository) {
        this.provinceRepository = provinceRepository;
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    @Override
    public void run(String... strings) throws Exception {
        InputStream inputStream = new ClassPathResource(PROVINCES_JSON).getInputStream();
        JsonNode rootNode = new ObjectMapper().readTree(inputStream);
        Iterator<Entry<String, JsonNode>> fieldsIterator = rootNode.fields();
        while (fieldsIterator.hasNext()) {
            Map.Entry<String, JsonNode> field = fieldsIterator.next();
            String name = field.getKey();
            Integer upperLeftX = field.getValue().findValue(UPPER_LEFT_FIELD).findValue(X_VALUE)
                                     .asInt();
            Integer upperLeftY = field.getValue().findValue(UPPER_LEFT_FIELD).findValue(Y_VALUE)
                                     .asInt();
            Integer bottomRightX = field.getValue().findValue(BOTTOM_RIGHT_FIELD).findValue(X_VALUE)
                                       .asInt();
            Integer bottomRightY = field.getValue().findValue(BOTTOM_RIGHT_FIELD).findValue(Y_VALUE)
                                       .asInt();
            Rectangle boundary = new Rectangle(new Point2DImpl(upperLeftX, upperLeftY),
                                                  new Point2DImpl(bottomRightX, bottomRightY));
            Province province = new Province(name, boundary);
            logger.info("Loading province {}", province);
            provinceRepository.insert(province);
        }
    }
}
