package com.resilva87.vrtest.loaders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.resilva87.vrtest.model.Properties;
import com.resilva87.vrtest.model.Property;
import com.resilva87.vrtest.repository.PropertyRepository;
import com.resilva87.vrtest.repository.ProvinceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.List;

@Component
@Order(2)
public class PropertiesLoader implements CommandLineRunner {

    public static final String PROPERTIES_JSON = "properties.json";
    private final Logger logger;
    private final PropertyRepository propertyRepository;
    private final ProvinceRepository provinceRepository;

    @Autowired
    public PropertiesLoader(PropertyRepository propertyRepository, ProvinceRepository provinceRepository) {
        this.propertyRepository = propertyRepository;
        this.provinceRepository = provinceRepository;
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    @Override
    public void run(String... strings) throws Exception {
        InputStream inputStream = new ClassPathResource(PROPERTIES_JSON).getInputStream();
        Properties properties = new ObjectMapper().readValue(inputStream, Properties.class);
        logger.info("Loading {} properties", properties.getTotalProperties());
        List<Property> propertiesList = properties.getProperties();
        for(Property property: propertiesList) {
            property.setProvinces(provinceRepository.findContaining(property));
        }
        propertyRepository.load(propertiesList);
    }
}
