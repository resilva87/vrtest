package com.resilva87.vrtest.service;

import com.resilva87.vrtest.model.Properties;
import com.resilva87.vrtest.model.Property;

import javax.validation.Valid;
import java.util.Optional;

public interface PropertyService {
    Property create(@Valid Property newProperty);
    Property findById(Long id);
    Properties findByRangeSearch(Integer ax, Integer bx, Integer ay, Integer by);
}
