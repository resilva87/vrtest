package com.resilva87.vrtest.service.impl;

import com.resilva87.vrtest.exception.PropertyNotFoundException;
import com.resilva87.vrtest.math.Point2DImpl;
import com.resilva87.vrtest.math.Rectangle;
import com.resilva87.vrtest.model.Properties;
import com.resilva87.vrtest.model.Property;
import com.resilva87.vrtest.model.Province;
import com.resilva87.vrtest.repository.PropertyRepository;
import com.resilva87.vrtest.repository.ProvinceRepository;
import com.resilva87.vrtest.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
@Validated
class PropertyServiceImpl implements PropertyService {

    private final PropertyRepository propertyRepository;
    private final ProvinceRepository provinceRepository;

    @Autowired
    public PropertyServiceImpl(PropertyRepository propertyRepository, ProvinceRepository provinceRepository) {
        this.propertyRepository = propertyRepository;
        this.provinceRepository = provinceRepository;
    }

    @Override
    public Property create(@Valid Property newProperty) {
        addMatchingProvinces(newProperty);
        return propertyRepository.create(newProperty);
    }

    @Override
    public Property findById(Long id) {
        return propertyRepository.findById(id).orElseThrow(() -> new PropertyNotFoundException(id));
    }

    @Override
    public Properties findByRangeSearch(Integer ax, Integer bx, Integer ay, Integer by) {
        Rectangle searchRect = new Rectangle(new Point2DImpl(ax, ay), new Point2DImpl(bx, by));
        List<Property> foundProperties = propertyRepository.rangeSearch(searchRect);
        return new Properties(foundProperties);
    }

    private void addMatchingProvinces(Property property) {
        List<Province> provinces = provinceRepository.findContaining(property);
        if(provinces.isEmpty()) {
            // TODO lançar exceção custom
            throw new IllegalArgumentException("Property " + property.getId() + " is not "
                    + "contained in any province");
        }
        property.setProvinces(provinces);
    }

}
