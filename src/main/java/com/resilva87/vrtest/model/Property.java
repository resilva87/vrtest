package com.resilva87.vrtest.model;

import com.fasterxml.jackson.annotation.*;
import com.resilva87.vrtest.math.Point2D;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

public class Property implements Point2D, Serializable {

    private static final long serialVersionUID = -2816267232873829891L;

    private Long id;

    private List<Province> provinces;

    @NotEmpty(message = "message.validation.title.empty")
    private final String title;

    @NotNull(message = "message.validation.price.null")
    @Min(value = 1, message = "message.validation.price.minimum")
    private final Integer price;

    @NotEmpty(message = "message.validation.description.empty")
    private final String description;

    @NotNull(message = "message.validation.latitude.null")
    @Min(value = 0, message = "message.validation.latitude.minimum")
    @Max(value = 1400, message = "message.validation.latitude.maximum")
    private Integer latitude;

    @NotNull(message = "message.validation.longitude.null")
    @Min(value = 0, message = "message.validation.longitude.minimum")
    @Max(value = 1000, message = "message.validation.longitude.maximum")
    private Integer longitude;

    @NotNull(message = "message.validation.beds.null")
    @Min(value = 1, message = "message.validation.beds.minimum")
    @Max(value = 5, message = "message.validation.beds.maximum")
    private final Integer beds;

    @NotNull(message = "message.validation.baths.null")
    @Min(value = 1, message = "message.validation.baths.minimum")
    @Max(value = 4, message = "message.validation.baths.maximum")
    private final Integer baths;

    @NotNull(message = "message.validation.squareMeters.null")
    @Min(value = 20, message = "message.validation.squareMeters.minimum")
    @Max(value = 240, message = "message.validation.squareMeters.maximum")
    private final Integer squareMeters;

    @JsonCreator
    public Property(@JsonProperty("title") String title,
                    @JsonProperty("price") Integer price,
                    @JsonProperty("description") String description,
                    @JsonProperty("x") Integer latitude,
                    @JsonProperty("y") Integer longitude,
                    @JsonProperty("beds") Integer beds,
                    @JsonProperty("baths") Integer baths,
                    @JsonProperty("squareMeters") Integer squareMeters) {
        this.title = title;
        this.price = price;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.beds = beds;
        this.baths = baths;
        this.squareMeters = squareMeters;
    }

    @JsonSetter("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonSetter
    public void setLat(Integer latitude) {
        this.latitude = latitude;
    }

    @JsonSetter
    public void setLong(Integer longitude) {
        this.longitude = longitude;
    }

    @JsonGetter
    public Long getId() {
        return this.id;
    }

    public String getTitle() {
        return title;
    }

    public Integer getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public Integer getBeds() {
        return beds;
    }

    public Integer getBaths() {
        return baths;
    }

    public Integer getSquareMeters() {
        return squareMeters;
    }

    @JsonGetter
    public List<Province> getProvinces() {
        return provinces;
    }

    public void setProvinces(List<Province> provinces) {
        this.provinces = provinces;
    }

    @JsonGetter
    @Override
    public Integer x() {
        return latitude;
    }

    @JsonGetter
    @Override
    public Integer y() {
        return longitude;
    }

}
