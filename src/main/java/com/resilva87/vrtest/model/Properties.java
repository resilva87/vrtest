package com.resilva87.vrtest.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class Properties {

    @JsonProperty
    private final Integer totalProperties;

    @JsonProperty
    private final List<Property> properties;

    @JsonCreator
    public Properties(@JsonProperty("totalProperties") Integer totalProperties,
                         @JsonProperty("properties") List<Property> properties) {
        this.totalProperties = totalProperties;
        this.properties = properties;
    }

    public Properties(List<Property> properties) {
        this(properties.size(), properties);
    }

    public Integer getTotalProperties() {
        return totalProperties;
    }

    public List<Property> getProperties() {
        return properties;
    }

}
