package com.resilva87.vrtest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import com.resilva87.vrtest.math.Rectangle;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

public class Province implements Serializable {

    private static final long serialVersionUID = -5093545825975392752L;

    public static Comparator<? super Province> defaultComparator =
        (Comparator<Province>) (o1, o2) -> Rectangle.defaultComparator
                                               .compare(
                                                   o1.boundary,
                                                   o2.boundary);
    private final String name;

    @JsonIgnore
    private final Rectangle boundary;

    public Province(String name, Rectangle boundary) {
        this.name = name;
        this.boundary = boundary;
    }

    @JsonValue
    public String getName() {
        return name;
    }

    public Rectangle getBoundary() {
        return boundary;
    }

    @Override
    public String toString() {
        return "Province{" +
                   "name='" + name + '\'' +
                   ", boundary=" + boundary +
                   '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Province province = (Province) o;
        return Objects.equals(name, province.name) &&
                   Objects.equals(boundary, province.boundary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, boundary);
    }
}
