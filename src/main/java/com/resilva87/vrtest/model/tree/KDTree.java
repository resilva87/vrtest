package com.resilva87.vrtest.model.tree;

import com.resilva87.vrtest.math.Axis;
import com.resilva87.vrtest.math.Point2D;

import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class KDTree {

    private Node root;

    private KDTree() {
    }

    public static KDTree empty() {
        return new KDTree();
    }

    public static KDTree fromPoints(List<? extends Point2D> points) {
        assert points != null;
        KDTree tree = new KDTree();
        tree.root = insertFromPointList(points, Axis.X);
        return tree;
    }

    private static Node insertFromPointList(List<? extends Point2D> points, Axis currentAxis) {
        if (points.isEmpty()) {
            return null;
        }
        points.sort(Point2D.getComparator(currentAxis));
        int length = points.size();
        int medianPos = length / 2;
        Point2D medianPoint = points.get(medianPos);
        if (length == 1) {
            return new Node(medianPoint, null, null);
        }
        List<? extends Point2D> leftPoints = points.subList(0, medianPos);
        List<? extends Point2D> rightPoints = points.subList(medianPos + 1, length);
        return new Node(medianPoint, insertFromPointList(leftPoints, Axis.cycle(currentAxis)),
                insertFromPointList(rightPoints, Axis.cycle(currentAxis)));
    }

    public int size() {
        return size(this.root);
    }

    public Node root() {
        return root;
    }

    public Node insert(Point2D newValue) {
        Node newNode = new Node(newValue);
        if (this.root == null) {
            this.root = newNode;
            return newNode;
        }
        Axis currentAxis = Axis.X;
        Node focusNode = this.root;
        Node parent;
        Comparator<Point2D> comparator;
        while (true) {
            parent = focusNode;
            comparator = Point2D.getComparator(currentAxis);
            Point2D currentValue = focusNode.getValue();
            if (comparator.compare(newValue, currentValue) <= 0) {
                focusNode = focusNode.getLeftNode();
                if(focusNode == null) {
                    break;
                }
            } else /*if (comparator.compare(newValue, currentValue) > 0)*/ {
                focusNode = focusNode.getRightNode();
                if(focusNode == null) {
                    break;
                }
            }
            currentAxis = Axis.cycle(currentAxis);
        }
        insertChildNode(parent, currentAxis, newNode);
        return newNode;
    }

    public void traverse(Consumer<Node> consumer) {
        traverse(this.root, consumer);
    }

    public boolean isEmpty() {
        return this.size() == 0;
    }

    private void insertChildNode(Node parent, Axis currentAxis, Node newNode) {
        Comparator<Point2D> comparator = Point2D.getComparator(currentAxis);
        if (comparator.compare(newNode.getValue(), parent.getValue()) > 0) {
            parent.setRightNode(newNode);
        } else {
            parent.setLeftNode(newNode);
        }
    }

    private int size(Node node) {
        return (node == null) ? 0 : size(node.getLeftNode()) + 1 + size(node.getRightNode());
    }

    private void traverse(Node node, Consumer<Node> consumer) {
        if(node == null){
            return;
        }
        traverse(node.getLeftNode(), consumer);
        consumer.accept(node);
        traverse(node.getRightNode(), consumer);
    }
}
