package com.resilva87.vrtest.model.tree;

import com.resilva87.vrtest.math.Point2D;

import java.util.Objects;

public class Node {
    private final Point2D value;
    private Node leftNode, rightNode;

    public Node(Point2D value) {
        this.value = value;
    }

    public Node(Point2D value, Node leftNode, Node rightNode) {
        this(value);
        this.leftNode = leftNode;
        this.rightNode = rightNode;
    }

    public Point2D getValue() {
        return value;
    }

    public Node getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(Node leftNode) {
        this.leftNode = leftNode;
    }

    public Node getRightNode() {
        return rightNode;
    }

    public void setRightNode(Node rightNode) {
        this.rightNode = rightNode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Node)) {
            return false;
        }
        Node node = (Node) o;
        return Objects.equals(value, node.value) &&
                Objects.equals(leftNode, node.leftNode) &&
                Objects.equals(rightNode, node.rightNode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, leftNode, rightNode);
    }
}
