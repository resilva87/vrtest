package com.resilva87.vrtest.model.tree;

import com.resilva87.vrtest.math.Axis;
import com.resilva87.vrtest.math.Point2D;
import com.resilva87.vrtest.math.Point2DImpl;
import com.resilva87.vrtest.math.Rectangle;

import java.util.ArrayList;
import java.util.List;

public class RangeQuery {

    private final Point2D upperLeft, bottomLeft, upperRight, bottomRight;

    public RangeQuery(Point2D upperLeft, Point2D bottomLeft, Point2D bottomRight,
                      Point2D upperRight) {
        this.upperLeft = upperLeft;
        this.bottomLeft = bottomLeft;
        this.upperRight = upperRight;
        this.bottomRight = bottomRight;
    }

    public RangeQuery(Rectangle rectangle) {
        this(rectangle.getUpperLeft(),
                new Point2DImpl(rectangle.getUpperLeft().x(), rectangle.getBottomRight().y()),
                rectangle.getBottomRight(),
                new Point2DImpl(rectangle.getBottomRight().x(), rectangle.getUpperLeft().y()));
    }

    public List<Point2D> search(KDTree kdTree) {
        return searchFrom(kdTree.root(), Axis.X);
    }

    private List<Point2D> searchFrom(Node node, Axis currentAxis) {
        List<Point2D> result = new ArrayList<>();
        if (node == null) {
            return result;
        }

        Point2D nodeValue = node.getValue();

        // if point is contained in the range query, we need to
        // add the point to the result and check both left and
        // right subtrees for additional points
        if (isPointInsideOrIntersectsRange(nodeValue)) {
            result.add(nodeValue);
            searchBothSubtrees(node, currentAxis, result);
        } else {
            // With the current axis, point is to the right of range query:
            // only search in the left subtree
            if (isPointRight(nodeValue, currentAxis)) {
                rangeQueryLeftSubtree(node, currentAxis, result);
            }
            // Or point is to the left of query range:
            // only search in right subtree
            else if (isPointLeft(nodeValue, currentAxis)) {
                rangeQueryRightSubtree(node, currentAxis, result);
            }
            // Point is neither to the left or to the right:
            // it intersects with query range in the current axis
            // so we must check both subtrees for additional points
            else {
                searchBothSubtrees(node, currentAxis, result);
            }
        }
        return result;
    }

    private void searchBothSubtrees(Node root, Axis currentAxis, List<Point2D> result) {
        rangeQueryLeftSubtree(root, currentAxis, result);
        rangeQueryRightSubtree(root, currentAxis, result);
    }

    private void rangeQueryRightSubtree(Node root, Axis currentAxis, List<Point2D> result) {
        rangeQuerySubtree(root.getRightNode(), currentAxis, result);
    }

    private void rangeQueryLeftSubtree(Node root, Axis currentAxis, List<Point2D> result) {
        rangeQuerySubtree(root.getLeftNode(), currentAxis, result);
    }

    private void rangeQuerySubtree(Node node, Axis currentAxis, List<Point2D> result) {
        if (node != null) {
            result.addAll(searchFrom(node, Axis.cycle(currentAxis)));
        }
    }

    private boolean isPointLeft(Point2D value, Axis currentAxis) {
        if (currentAxis == Axis.X) {
            return value.x() < bottomLeft.x();
        }
        return value.y() < bottomLeft.y();
    }

    private boolean isPointRight(Point2D value, Axis currentAxis) {
        if (currentAxis == Axis.X) {
            return value.x() > bottomRight.x();
        }
        return value.y() > upperRight.y();
    }

    private boolean isPointInsideOrIntersectsRange(Point2D point2D) {
        return point2D.x() >= bottomLeft.x() && point2D.x() <= upperRight.x() &&
                point2D.y() >= bottomLeft.y() && point2D.y() <= upperLeft.y();
    }
}

