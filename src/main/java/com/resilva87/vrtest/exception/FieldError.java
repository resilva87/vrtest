package com.resilva87.vrtest.exception;

import org.apache.commons.lang.StringUtils;

import javax.validation.ConstraintViolation;

public class FieldError {

    private final String field;
    private final String code;
    private final String message;

    public FieldError(String field, String code, String message) {
        this.field = field;
        this.code = code;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static FieldError of(ConstraintViolation<?> constraintViolation, String message) {
        String field = StringUtils.substringAfterLast(
            constraintViolation.getPropertyPath().toString(), ".");

        return new FieldError(field,
                                 constraintViolation.getMessageTemplate(),
                                 message);

    }
}
