package com.resilva87.vrtest.exception;

public interface MappedException {
    String getField();
    String getCode();
}
