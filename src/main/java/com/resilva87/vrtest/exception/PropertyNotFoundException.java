package com.resilva87.vrtest.exception;

public class PropertyNotFoundException extends RuntimeException implements MappedException {

    private static final String MESSAGE_ERROR_ID_NOTFOUND = "message.error.id.notfound";
    private static final String FIELD_ID = "id";

    private  final Long propertyId;

    public PropertyNotFoundException(Long propertyId) {
        this.propertyId = propertyId;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    @Override
    public String getField() {
        return FIELD_ID;
    }

    @Override
    public String getCode() {
        return MESSAGE_ERROR_ID_NOTFOUND;
    }
}
