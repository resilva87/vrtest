package com.resilva87.vrtest.web.controller;

import com.resilva87.vrtest.exception.PropertyNotFoundException;
import com.resilva87.vrtest.model.Properties;
import com.resilva87.vrtest.model.Property;
import com.resilva87.vrtest.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/properties")
public class PropertyController {

    private final PropertyService propertyService;

    @Autowired
    public PropertyController(PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Property findById(@NotNull @PathVariable Long id) {
        return propertyService.findById(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Properties findByRangeSearch(
            @RequestParam("ax") Integer ax,
            @RequestParam("ay") Integer ay,
            @RequestParam("bx") Integer bx,
            @RequestParam("by") Integer by) {
        return propertyService.findByRangeSearch(ax, bx, ay, by);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Property create(@NotNull @RequestBody Property property) {
        return propertyService.create(property);
    }

}
