package com.resilva87.vrtest.web.handler;

import com.resilva87.vrtest.exception.FieldError;
import com.resilva87.vrtest.exception.MappedException;
import com.resilva87.vrtest.exception.PropertyNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@RestControllerAdvice
public class DefaultExceptionHandler {

    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;

    @Autowired
    public DefaultExceptionHandler(MessageSource messageSource,
                                   LocaleResolver localeResolver) {
        this.messageSource = messageSource;
        this.localeResolver = localeResolver;
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ExceptionHandler(PropertyNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public FieldError handlePropertyNotFoundException(PropertyNotFoundException e,
                                                      HttpServletRequest request) {
        final Locale locale = localeResolver.resolveLocale(request);
        return new FieldError(e.getField(), e.getCode(), getMessage(e, locale, e.getPropertyId()));
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public List<FieldError> handleConstraintViolationException(ConstraintViolationException ex, HttpServletRequest request) {
        final Locale locale = localeResolver.resolveLocale(request);
        return ex.getConstraintViolations().stream()
                .map(violation -> FieldError.of(violation, getMessage(violation.getMessageTemplate(), locale)))
                .collect(Collectors.toList());
    }

    private String getMessage(MappedException mappedException, Locale locale, Object... params) {
        return MessageFormat.format(getMessageTemplate(mappedException.getCode(), locale), params);
    }

    private String getMessage(String code, Locale locale, Object... params) {
        return MessageFormat.format(getMessageTemplate(code, locale), params);
    }

    private String getMessageTemplate(String messageCode, Locale locale) {
        try {
            return this.messageSource.getMessage(messageCode, null, locale);
        } catch (NoSuchMessageException noSuchMessageException) {
            return noSuchMessageException.getMessage();
        }
    }
}
