package com.resilva87.vrtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VrtestApplication {

    public static void main(String[] args) {
        SpringApplication.run(VrtestApplication.class, args);
    }
    
}
