package com.resilva87.vrtest.math;

public enum Axis {
    X,
    Y;

    public static Axis cycle(Axis axis) {
        return axis == X ? Y : X;
    }
}
