package com.resilva87.vrtest.math;

import java.util.Comparator;
import java.util.Objects;

public class Rectangle {

    public static Comparator<Rectangle> defaultComparator = (o1, o2) -> {
        int byX = Point2D.getComparator(Axis.X).compare(o1.bottomRight, o2.bottomRight);
        int byY = Point2D.getComparator(Axis.Y).compare(o1.bottomRight, o2.bottomRight);
        if(byY == 0) {
            return byX;
        }
        return byY;
    };

    private final Point2D bottomRight, upperLeft;

    public Rectangle(Point2D upperLeft, Point2D bottomRight) {
        this.upperLeft = upperLeft;
        this.bottomRight = bottomRight;
    }

    public Point2D getBottomRight() {
        return bottomRight;
    }

    public Point2D getUpperLeft() {
        return upperLeft;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Rectangle)) {
            return false;
        }
        Rectangle rectangle = (Rectangle) o;
        return
            Objects.equals(bottomRight, rectangle.bottomRight) &&
                Objects.equals(upperLeft, rectangle.upperLeft);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bottomRight, upperLeft);
    }

    public boolean contains(Point2D point2D) {
        return point2D.y() >= bottomRight.y() && point2D.y() <= upperLeft.y() &&
                point2D.x() >= upperLeft.x() && point2D.x() <= bottomRight.x();
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "bottomRight=" + bottomRight +
                ", upperLeft=" + upperLeft +
                '}';
    }
}