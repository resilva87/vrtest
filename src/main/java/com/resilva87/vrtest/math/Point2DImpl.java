package com.resilva87.vrtest.math;

import java.util.Objects;

public class Point2DImpl implements Point2D {

    private final Integer x, y;

    public Point2DImpl(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public Integer x() {
        return this.x;
    }

    @Override
    public Integer y() {
        return this.y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Point2DImpl)) {
            return false;
        }
        Point2DImpl point2DImpl = (Point2DImpl) o;
        return Objects.equals(x, point2DImpl.x) &&
                   Objects.equals(y, point2DImpl.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Point2DImpl{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
