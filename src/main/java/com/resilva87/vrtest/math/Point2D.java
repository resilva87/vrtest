package com.resilva87.vrtest.math;

import java.util.Comparator;

public interface Point2D {
    static Comparator<Point2D> getComparator(Axis currentAxis) {
        return (currentAxis == Axis.X) ? Comparator.comparing(Point2D::x) : Comparator.comparing(Point2D::y);
    }
    Integer x();
    Integer y();
}
