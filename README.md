# Spotippos - Desafio Backend #

Este repositório contem a implementação do desafio backend VivaReal (https://github.com/VivaReal/code-challenge/blob/master/backend.md), onde é possível criar e buscar propriedades, espalhadas em províncias, em um espaço 2D.

## Tecnologias Requeridas ##
* Java 8
* Maven 3
* Docker

## Deploy ##

Para construir a aplicação:
```
git clone https://resilva87@bitbucket.org/resilva87/vrtest.git
cd vrtest
mvn clean install (executa testes unitários, integrados e gera imagem Docker)
```

Para executar existem 2 opções:
```
java -jar target/vrtest-1.0.0.jar (executa a aplicação Spring Boot utilizando a porta 8080)
```
ou
```
docker run -d --name spotippos -p PORTA_MAQUINA:8080 vrtest/spotippos (cria um container a partir da imagem Docker criada no processo de build, utilizando a porta PORTA_MAQUINA)
```

## API ##

### 1-) Criar nova propriedade
```
 POST /properties
```

**Request Body**
```json
{
  "x": 222,
  "y": 444,
  "title": "Imóvel código 1, com 5 quartos e 4 banheiros",
  "price": 1250000,
  "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  "beds": 4,
  "baths": 3,
  "squareMeters": 210
}
```

Response **201 Created**
```json
{
  "title": "Imóvel código 1, com 5 quartos e 4 banheiros",
  "price": 1250000,
  "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  "x": 222,
  "y": 444,
  "beds": 4,
  "baths": 3,
  "squareMeters": 210,
  "provinces": [
    "Scavy"
  ],
  "id": 8002
}
```

Response **400 Bad Request**
```json
[
  {
    "field": "title",
    "code": "message.validation.title.empty",
    "message": "Property title must not be empty"
  },
  {...}
]
```

### 2-) Buscar propriedade por ID

Request:
```
 GET /properties/{id}
```

Response **200 OK**
```json
{
  "title": "Imóvel código 10, com 3 quartos e 2 banheiros.",
  "price": 661000,
  "description": "In reprehenderit sit dolor nostrud enim nisi proident non deserunt incididunt pariatur sunt. Adipisicing nisi fugiat commodo cillum ea aute anim magna eu magna duis officia.",
  "x": 304,
  "y": 225,
  "beds": 3,
  "baths": 2,
  "squareMeters": 64,
  "provinces": [
    "Scavy"
  ],
  "id": 10
}
```

Response **404 Not Found**
```json
{
  "field": "id",
  "code": "message.error.id.notfound",
  "message": "Property 8,838 does not exist"
}
```

### 3-) Buscar propriedades em uma região

Request:
```
 GET /properties?ax={integer}&ay={integer}&bx={integer}&by={integer}
```

Response **200 OK**
```json
{
  "totalProperties": 1746,
  "properties": [
    {
      "title": "Imóvel código 1415, com 1 quartos e 1 banheiros.",
      "price": 420000,
      "description": "Excepteur commodo minim pariatur enim. Amet adipisicing anim pariatur esse fugiat voluptate consequat amet incididunt.",
      "x": 505,
      "y": 505,
      "beds": 1,
      "baths": 1,
      "squareMeters": 38,
      "provinces": [
        "Gode",
        "Ruja"
      ],
      "id": 1415
    },
    {...}
 ]
}
```

## Implementação ##

A aplicação foi desenvolvida utilizando o framework Spring Boot, de modo a agilizar a facilitar a estrutura e funcionalidades presentes no sistema. De modo geral, buscou organizar o código de maneira clara, com responsabilidades separadas nos pacotes e classes. As partes mais importantes são:

* **com.resilva87.vrtest.configuration**: Pacote com as configurações dos validadores e recursos de mensagem
* **com.resilva87.vrtest.loaders**: Pacote com os carregadores de arquivo json para a aplicação
* **com.resilva87.vrtest.model.tree**: Pacote com classes para implementar uma kd-tree. Esta árvore foi utilizada de forma a otimizar a busca de propriedades por região (busca "range search").