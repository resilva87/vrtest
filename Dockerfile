FROM openjdk:8-jre-alpine
MAINTAINER Renato Silva <resilva87@outlook.com>

ENTRYPOINT ["/usr/bin/java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/usr/share/vrtest/vrtest.jar"]

# Add the service itself
ADD target/vrtest-1.0.0.jar /usr/share/vrtest/vrtest.jar